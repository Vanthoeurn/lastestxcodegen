//
//  ViewController.swift
//  MyApp
//
//  Created by Luckea Van on 1/11/23.
//

import UIKit
import SnapKit

class ViewController: UIViewController {

    lazy var buttonView: UIButton = {
        let btn = UIButton()
        btn.setTitle("Button", for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.backgroundColor = .systemOrange
        btn.layer.cornerRadius = 10
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBlue
        view.addSubview(buttonView)
        buttonView.snp.makeConstraints { make in
            make.centerX.centerY.equalToSuperview()
            make.width.equalTo(100)
            make.height.equalTo(40)
        }
    }
}

